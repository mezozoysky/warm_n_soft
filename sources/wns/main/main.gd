extends Node

var _start_menu_ui_scene : PackedScene = null
var _game_scene : PackedScene = null
var _game_top_ui_scene : PackedScene = null


func _ready() -> void:
    print(get_path(), ' is ready')


func _on_phase_switch_phase_stacked(phase) -> void:
    if phase == 'start_menu':
        _start_menu_ui_scene = load('res://wns/start_menu/start_menu.tscn')
    elif phase == 'game':
        _game_scene = load('res://wns/game/game.tscn')
        _game_top_ui_scene = load('res://wns/game/game_top_ui.tscn')
    else:
        printerr(get_path(), ': Unknown phase stacking: "', phase, '"')


func _on_phase_switch_phase_unstacked(phase) -> void:
    if phase == 'start_menu':
        _start_menu_ui_scene = null
    elif phase == 'game':
        _game_scene = null
        _game_top_ui_scene = null
    else:
        printerr(get_path(), ': Unknown phase unstacking: "', phase, '"')


func _on_phase_switch_enter_phase(phase, prev) -> void:
    print(get_path(),
          ': Entering "', phase, '" phase, prev phase is "', prev, '"')
    if !phase:
        print(get_path(), ': Quiting the game ...')
        get_tree().quit()
    elif phase == 'start_menu':
        var start_menu : = _start_menu_ui_scene.instance()
        add_child(start_menu)
    elif phase == 'game':
        var game : = _game_scene.instance()
        var game_top_ui : = _game_top_ui_scene.instance()
        add_child(game)
        add_child(game_top_ui)
    else:
        printerr(get_path(), ': Unknown phase entering: "', phase, '"')


func _on_phase_switch_quit_phase(phase, next) -> void:
    print(get_path(),
          ': Quiting "', phase, '" phase, next phase is "', next, '"')
    if !phase:
        print(get_path(), ': Quiting null-phase, so game is just launched')
    elif phase == 'start_menu':
        $start_menu.queue_free()
        remove_child($start_menu)
    elif phase == 'game':
        $game_top_ui.queue_free()
        remove_child($game_top_ui)
        $game.queue_free()
        remove_child($game)
    else:
        printerr(get_path(), ': Unknown scene quiting: "', phase, '"')
