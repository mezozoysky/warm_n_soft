extends Control

signal first_move_side_selected

func _ready():
    pass


func show_modal_view(view_name: String) -> void:
    hide_all_modals()
    var modal_view : Node = get_node('modals/center/' + view_name)
    modal_view.setup_modal_view()
    modal_view.show()
    $modals.show()

func hide_modal_view(view_name: String) -> void:
    $modals.hide()
    $"modals/center".get_node(view_name).hide()


func hide_all_modals() -> void:
    for ch in $"modals/center".get_children():
        ch.hide()

func swap_sides() -> void:
    var tmp_side = $side_ui_0.side
    $side_ui_0.side = $side_ui_1.side
    $side_ui_1.side = tmp_side

#func _on_who_is_first_modal_closed():
#    $"../phase_switch".pop()


func _on_who_is_first_first_move_side_selected(side) -> void:
    emit_signal('first_move_side_selected', side)
