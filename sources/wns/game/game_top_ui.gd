extends Control





func _ready():
    pass




func _on_exit_game_pressed():
    var switch : = commons.get_main_switch()
    switch.pop_until('start_menu')




func _on_restart_game_pressed():
    commons.get_game().restart_game()


func _on_swap_sides_pressed():
    var game_ui = commons.get_game().get_node_or_null('ui')
    if game_ui != null:
        game_ui.swap_sides()
