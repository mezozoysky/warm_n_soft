extends VBoxContainer

signal first_move_side_selected

var _first_move_side_selected


func _ready():
    _update_view()

func setup_modal_view() -> void:
    var game = commons.get_game()
    if game == null:
        printerr(get_path(), 'Failed to setup modal view: no game')
        return
    _first_move_side_selected = game.first_move_side
    _update_view()
    $"hbox/roll".grab_focus()

func _on_exit_pressed():
    commons.get_main_switch().pop()


func _on_select_warm_pressed():
    if _first_move_side_selected == Commons.Side.WARM:
        return
    _first_move_side_selected = Commons.Side.WARM
    _update_view()


func _on_select_soft_pressed():
    if _first_move_side_selected == Commons.Side.SOFT:
        return
    _first_move_side_selected = Commons.Side.SOFT
    _update_view()


func _on_roll_pressed():
    pass # Replace with function body.


func _on_ok_pressed():
    emit_signal('first_move_side_selected', _first_move_side_selected)

func _update_view() -> void:
    var game = commons.get_game()
    if game == null:
        return

    if _first_move_side_selected == Commons.Side.WARM:
        if game.near_board_side == Commons.Side.WARM:
            $near_side_selected_mark.text = 'warm side moves first'
            $far_side_selected_mark.text = ''
        else: # near side is SOFT
            $far_side_selected_mark.text = 'warm side moves first'
            $near_side_selected_mark.text = ''
    else: # SOFT selected
        if game.near_board_side == Commons.Side.WARM:
            $far_side_selected_mark.text = 'soft side moves first'
            $near_side_selected_mark.text = ''
        else: # near side if SOFT
            $near_side_selected_mark.text = 'soft side moves first'
            $far_side_selected_mark.text = ''
