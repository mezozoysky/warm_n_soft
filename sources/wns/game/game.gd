extends Node2D

export(Commons.Side) var first_move_side_initial : = Commons.Side.WARM
export(Commons.Side) var near_board_side_initial : = Commons.Side.WARM
var first_move_side setget , get_first_move_side
var near_board_side setget , get_near_board_side
export(int) var warm_pawns_initial : = 7
export(int) var soft_pawns_initial : = 7
var warm_pawns_in_game : int
var soft_pawns_in_game : int
var warm_pawns_on_board : = []
var soft_pawns_on_board : = []
var _first_move_is_done : bool = false


func _ready() -> void:
    _setup_game()

    $board.position \
            = get_viewport_rect().size / 2 \
            - $board.get_used_rect().size * $board.cell_size / 2

    restart_game()

func get_first_move_side():
    return first_move_side


func get_near_board_side():
    return near_board_side


func get_far_board_side():
    if near_board_side == Commons.Side.WARM:
        return Commons.Side.SOFT
    return Commons.Side.WARM


func _setup_game() -> void:
    near_board_side = near_board_side_initial
    first_move_side = first_move_side_initial


func _initialize_game() -> void:
    _first_move_is_done = false
    warm_pawns_in_game = warm_pawns_initial
    soft_pawns_in_game = soft_pawns_initial
    print(get_path(), ': game initialized')


func restart_game() -> void:
    _initialize_game()
    if !false: # if first move side is undefined
        if $phase_switch.get_current_phase() != 'who_is_first':
            $phase_switch.push('who_is_first')



func _on_phase_switch_enter_phase(phase: String, prev: String):
    if phase.empty():
        commons.get_main_switch().pop()
    elif phase == 'the_move':
        pass
    elif phase in $phase_switch.phases:
        $ui.show_modal_view(phase)
    else:
        printerr(get_path(), ': Entering unknown game phase "', phase,
                 '", available phases: ', $phase_switch.phases)


func _on_phase_switch_quit_phase(phase: String, next: String):
    if phase.empty():
        pass
    elif phase == 'the_move':
        pass
    elif phase in $phase_switch.phases:
        $ui.hide_modal_view(phase)
    else:
        printerr(get_path(), ': Quiting unknown game phase "', phase,
                 '", available phases: ', $phase_switch.phases)


func _on_ui_first_move_side_selected(side):
    if $phase_switch.get_current_phase() == 'who_is_first':
        $phase_switch.pop()
    if first_move_side == side:
        return
    first_move_side = side
