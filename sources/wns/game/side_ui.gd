tool
extends Control


export(Commons.Side) var side : = Commons.Side.WARM setget set_side, get_side
export(Color) var warm_color : = Color(0.6, 0.4, 0.2, 0.2)
export(Color) var soft_color : = Color(0.2, 0.4, 1.0, 0.2)
export(bool) var is_near : bool = true


func _ready():
    _setup_side_ui()
    _initialize_side_ui()

func _setup_side_ui() -> void:
    var crect : ColorRect = $color_rect
    crect.anchor_left = 0
    crect.anchor_top = 0
    crect.anchor_right = 1
    crect.anchor_bottom = 1

func _initialize_side_ui() -> void:
    if side == Commons.Side.WARM:
        $color_rect.color = warm_color
    else:
        $color_rect.color = soft_color

    if is_near:
        $pawns_await.anchor_left = 0
        $pawns_await.anchor_top = 0
        $pawns_await.anchor_right = 0
        $pawns_await.anchor_bottom = 1
        $pawns_await.grow_vertical = Control.GROW_DIRECTION_BEGIN
        $pawns_await.grow_horizontal = Control.GROW_DIRECTION_END
        $pawns_await.alignment = BoxContainer.ALIGN_END

        $pawns_won.anchor_left = 1
        $pawns_won.anchor_top = 0
        $pawns_won.anchor_right = 1
        $pawns_won.anchor_bottom = 1
        $pawns_won.grow_vertical = Control.GROW_DIRECTION_BEGIN
        $pawns_won.grow_horizontal = Control.GROW_DIRECTION_BEGIN
        $pawns_won.alignment = BoxContainer.ALIGN_END

        $pawns_on_board.anchor_left = 0
        $pawns_on_board.anchor_top = 1
        $pawns_on_board.anchor_right = 1
        $pawns_on_board.anchor_bottom = 1
        $pawns_on_board.grow_vertical = Control.GROW_DIRECTION_BEGIN
    else:
        $pawns_await.anchor_left = 0
        $pawns_await.anchor_top = 0
        $pawns_await.anchor_right = 0
        $pawns_await.anchor_bottom = 1
        $pawns_await.grow_vertical = Control.GROW_DIRECTION_END
        $pawns_won.grow_horizontal = Control.GROW_DIRECTION_END
        $pawns_await.alignment = BoxContainer.ALIGN_BEGIN

        $pawns_won.anchor_left = 1
        $pawns_won.anchor_top = 0
        $pawns_won.anchor_right = 1
        $pawns_won.anchor_bottom = 1
        $pawns_won.grow_vertical = Control.GROW_DIRECTION_END
        $pawns_won.grow_horizontal = Control.GROW_DIRECTION_BEGIN
        $pawns_won.alignment = BoxContainer.ALIGN_BEGIN

        $pawns_on_board.anchor_left = 0
        $pawns_on_board.anchor_top = 0
        $pawns_on_board.anchor_right = 1
        $pawns_on_board.anchor_bottom = 0
        $pawns_on_board.grow_vertical = Control.GROW_DIRECTION_END

    var pawns_initial : int = game_state.get_pawns_initial(side)
    var pawns_in_game : int = game_state.get_pawns_in_game(side)
    var pawns_on_board : int = game_state.get_pawns_on_board(side)
    var pawns_faced : int = game_state.get_pawns_faced(side)

    var pawns_won : int = pawns_initial - pawns_in_game
    var pawns_await : int = pawns_in_game - pawns_on_board
    var pawns_blank : int = pawns_on_board - pawns_faced

    var side_str : = 'warm' if side == Commons.Side.WARM else 'soft'
    var blank_prefab_name : = 'pawn_' + side_str + '_blank_icon'
    var faced_prefab_name : = 'pawn_' + side_str + '_faced_icon'
    for ch in $pawns_await.get_children():
        $pawns_await.remove_child(ch)
        ch.queue_free()
    for i in range(pawns_await):
        $pawns_await.add_child(commons.prefabs[blank_prefab_name].instance())
    for ch in $pawns_won.get_children():
        $pawns_won.remove_child(ch)
        ch.queue_free()
    for i in range(pawns_won):
        $pawns_won.add_child(commons.prefabs[faced_prefab_name].instance())

    for ch in $pawns_on_board/box.get_children():
        $pawns_on_board/box.remove_child(ch)
        ch.queue_free()
    for i in range(pawns_blank):
        $pawns_on_board/box.add_child(commons.prefabs[blank_prefab_name].instance())
    $pawns_on_board/box.add_child(VSeparator.new())
    for i in range(pawns_faced):
        $pawns_on_board/box.add_child(commons.prefabs[faced_prefab_name].instance())


func set_side(the_side) -> void:
    var should_init : = false
    if the_side != side:
        should_init = true
    side = the_side
    if should_init:
        _initialize_side_ui()

func get_side():
    return side
