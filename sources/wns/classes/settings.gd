extends Node
class_name Settings


var near_board_side_default = Commons.Side.WARM

var _game_rules_selected : String = ''
var _game_rules : GameRules = null


func _ready():
    if not _game_rules_selected:
        _game_rules_selected = commons.get_rules_supported()[0]['prefab_name']
    if not _game_rules:
        _game_rules = load('res://wns/rules/' + _game_rules_selected + '.tscn').instance()
        _game_rules.name = 'rules'
        add_child(_game_rules)
