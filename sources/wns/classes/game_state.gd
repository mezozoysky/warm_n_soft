extends Node
class_name GameState


func _ready():
    pass

func get_pawns_initial(side) -> int:
    return 7

func get_pawns_in_game(side) -> int:
    if side == Commons.Side.WARM:
        return 5
    return 6

func get_pawns_on_board(side) -> int:
    if side == Commons.Side.WARM:
        return 3
    return 4

func get_pawns_faced(side) -> int:
    return 2
