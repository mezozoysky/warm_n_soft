extends Node
class_name GameRules

enum FirstMoveSideSelect { WARM, SOFT, NO_CARE, }
export(FirstMoveSideSelect) var first_move_side_select : = FirstMoveSideSelect.NO_CARE
export(Commons.Side) var first_move_side_default : = Commons.Side.WARM

export(NodePath) var selector : NodePath = 'selector'
var _selector : Node = null

func _ready() -> void:
    if not selector:
        selector = 'selector'
    _selector = get_node(selector)

