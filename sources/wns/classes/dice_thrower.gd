extends Node
class_name DiceThrower

export(int) var dices_in_roll : int = 3
export(int) var dice_min_value : int = 0
export(int) var dice_max_value : int = 1

var _rngs : = []

func _ready() -> void:
    if dices_in_roll == 0:
        printerr(get_path(),
                 ': Rolling 0 dices: makes no sense')
        return
    if dice_max_value < dice_min_value:
        printerr(get_path(),
                 ': dice min value (', dice_min_value,
                 ') can not be greater then dice max value (', dice_max_value,
                 ')')
        return
    randomize()
    var rng : RandomNumberGenerator = null
    for _dice in range(0, dices_in_roll):
        rng = RandomNumberGenerator.new()
        rng.seed = randi()
        rng.randomize()
        _rngs.append(rng)

func roll_dices() -> int:
    #var the_roll : Array = []
    var roll_sum : int = 0
    var tmp_rnd : int
    for dice in range(0, dices_in_roll):
        tmp_rnd = _rngs[dice].randi_range(dice_min_value, dice_max_value)
        #the_roll.append(tmp_rnd)
        roll_sum += tmp_rnd
    return roll_sum
