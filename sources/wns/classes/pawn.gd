tool
extends Sprite
class_name Pawn


export(commons.Side) var side setget set_pawn_side
enum Spin {BLANK, FACE}
export(Spin) var spin setget set_pawn_spin
export(int) var cell_width : = 192


func _ready() -> void:
    region_enabled = true
    _update_texture()


func set_pawn_side(side_) -> void:
    side = side_
    _update_texture()


func set_pawn_spin(spin_) -> void:
    spin = spin_
    _update_texture()


func _update_texture() -> void:
    var idx_x : int = 0
    var idx_y : int = 0
    if side == Commons.Side.WARM and spin == Spin.FACE:
        idx_x = 1
        idx_y = 1
    elif side == Commons.Side.WARM:
        idx_x = 0
        idx_y = 1
    elif spin == Spin.FACE:
        idx_x = 1
        idx_y = 0

    region_rect = Rect2(0 + cell_width * idx_x,
                        0 + cell_width * idx_y,
                        cell_width, cell_width)


func _process(delta):
    pass
