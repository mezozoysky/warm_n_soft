extends Node
class_name Commons


enum Side {WARM, SOFT}

var prefabs : Dictionary = {}

func _ready():
    var prefab : PackedScene = load('res://wns/game/pawn_warm_blank_icon.tscn')
    prefabs['pawn_warm_blank_icon'] = prefab
    prefab = load('res://wns/game/pawn_warm_faced_icon.tscn')
    prefabs['pawn_warm_faced_icon'] = prefab
    prefab = load('res://wns/game/pawn_soft_blank_icon.tscn')
    prefabs['pawn_soft_blank_icon'] = prefab
    prefab = load('res://wns/game/pawn_soft_faced_icon.tscn')
    prefabs['pawn_soft_faced_icon'] = prefab


func get_main() -> Node:
    return $'/root/main' as Node


func get_main_switch() -> PhaseSwitch:
    return $"/root/main/phase_switch" as PhaseSwitch


func get_game() -> Node2D:
    var game = get_node_or_null('/root/main/game') as Node2D
    if game == null:
        printerr(get_path(), ': game node is not found')
    return game


func get_rules_supported() -> Array:
    return [
                {
                    'name' : 'Mesozoic (default)',
                    'prefab_name' : 'mesozoic',
                },
                {
                    'name' : 'One More',
                    'prefab_name' : 'onemore',
                },
           ]
