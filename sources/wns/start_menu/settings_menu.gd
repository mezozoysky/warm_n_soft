extends Panel



func _ready():
    pass

func setup_menu() -> void:
    $"menu_box/video".grab_focus()

func _on_back_pressed():
    var switch : PhaseSwitch = get_node('../phase_switch')
    switch.pop()
