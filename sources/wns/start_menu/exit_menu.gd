extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    pass

func setup_menu() -> void:
    $"menu_box/buttons_box/exit".grab_focus()

func _on_cancel_pressed():
    #var switch : PhaseSwitch = get_node('../phase_switch')
    #switch.pop()
    $"../phase_switch".pop()


func _on_exit_pressed():
    print(get_path(), ': Exit confirmed')
    $"/root/main/phase_switch".drop()
