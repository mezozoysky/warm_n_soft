extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
    _center_menu($settings_menu)
    $settings_menu.hide()
    _center_menu($exit_menu)
    $exit_menu.hide()
    _center_menu($main_menu)
    $main_menu.show()
    show()


func _center_menu(menu) -> void:
    menu.rect_position = get_viewport_rect().size / 2 - menu.rect_size / 2


func _on_phase_switch_enter_phase(phase, prev):
    if phase == '':
        return # no handling for entrering null-phase
    var menu : Control = get_node_or_null(phase)
    if menu == null:
        printerr(get_path(),
                 ': Failure while entering phase: "', phase, '"',
                 '; available phases: ', $phase_switch.phases)
        return
    menu.setup_menu()
    menu.show()



func _on_phase_switch_quit_phase(phase, next):
    if phase == '':
        return # no handling for quiting null-phase
    var menu : Control = get_node_or_null(phase)
    if menu == null:
        printerr(get_path(),
                 ': Failure while quiting phase: "', phase, '"',
                 '; available phases: ', $phase_switch.phases)
        return
    menu.hide()
