extends Panel

func _ready() -> void:
    pass

func setup_menu() -> void:
    $"menu_box/play".grab_focus()

func _on_play_pressed():
    $"/root/main/phase_switch".push('game')


func _on_settings_pressed():
    $"../phase_switch".push('settings_menu')


func _on_exit_pressed():
    print(get_path(), ': Exit requested')
    if !true: # if "exit configrmation" option is true
        $"../phase_switch".push('exit_menu')
    $"/root/main/phase_switch".drop()
