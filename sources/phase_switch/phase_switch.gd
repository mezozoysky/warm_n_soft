extends Node
class_name PhaseSwitch

signal quit_phase
signal enter_phase
signal phase_stacked
signal phase_unstacked


export (Array, String) var phases : = ['start',]
export (String) var start_phase : = 'start'
var _stack : = []
var _stacked_list : = []
var _unstacked_list : = []
var _curr_phase : String
var _prev_phase : String
var _is_switching : bool = false


func _ready() -> void:
    if not start_phase in phases:
        printerr(get_path(),
                 ': start phase "',
                 start_phase,
                 '" is unknown, available phases: ',
                 phases)
        return
    push(start_phase)

func push(phase: String) -> void:
    if not phase in phases:
        print(get_path(),
              ': Failed to push phase: "',
              phase,
              '", available phases: ',
              phases)
        return
    var stacking : bool = not phase in _stack
    _stack.push_back(phase)
    if stacking:
        _stacked_list.push_back(phase)
    _is_switching = true

func pop() -> void:
    if not _stack:
        printerr(get_path(), ': Failed to pop phase, stack is empty')
        return
    var phase: String = _stack.pop_back()
    if not phase in _stack:
        _unstacked_list.push_back(phase)
    _is_switching = true

func replace(phase: String) -> void:
    if not phase in phases:
        printerr(get_path(),
                 ': Failed to replace with phase: "', phase,
                 '", available phases: ', phases)
        return
    pop()
    push(phase)

func drop() -> void:
    if not _stack:
        printerr(get_path(), ': Failed to drop phases, stack is empty')
        return
    while _stack:
        pop()

func get_top_phase() -> String:
    return _stack.back() if _stack else ''

func get_current_phase() -> String:
    return _curr_phase

func get_previous_phase() -> String:
    return _prev_phase

func is_phase_stacked(phase: String) -> bool:
    return phase in _stack

func pop_until(phase: String) -> void:
    if not is_phase_stacked(phase):
        printerr(get_path(),
                 ': Failed to pop until "', phase, '";',
                 ' phase isnt stacked')
        return
    while phase != get_top_phase():
        pop()
    _is_switching = true

func _switch() -> void:
    if !_is_switching:
        return

    if _curr_phase == get_top_phase():
        pass
    else:
        emit_signal('quit_phase', _curr_phase, get_top_phase())
        while _unstacked_list:
            var phase : String = _unstacked_list.pop_front()
            emit_signal('phase_unstacked', phase)
        while _stacked_list:
            var phase = _stacked_list.pop_front()
            emit_signal('phase_stacked', phase)
        _prev_phase = _curr_phase
        _curr_phase = get_top_phase()
        emit_signal('enter_phase', _curr_phase, _prev_phase)

    _is_switching = false

func _process(delta) -> void:
    if _is_switching:
        call_deferred('_switch')
